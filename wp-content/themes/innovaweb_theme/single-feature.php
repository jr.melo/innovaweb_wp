<?php
get_header();

while (have_posts()) {
    the_post();

?>
    <div class="container">
        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8 column_single">
                <!-- Title -->
                <hr>
                <h1>
                    <center><?php the_title(); ?></center>
                </h1>
                <hr>

                <!-- Post Content -->
                <p><?php the_content() ?></p>

                <!-- Date/Time -->
                <hr>
                <p><b>Fecha Publicación:</b> <?php echo the_date(); ?></br><b>Hora:</b> <?php echo the_time(); ?>
                <div style="text-align:right"><?php previous_post_link(); echo "  ||  "; next_post_link();?></div>
                <hr>
                    <center><a href="<?php echo site_url("/features") ?>" class="btn btn-primary">Regresar</a></center>
                </p>
                <hr>

            </div>
                <!-- Sidebar Widgets Column -->
            <div class="col-md-4 column_single">
                    
                    <?php

                        //Novedades Custom Query
                        $args = array(
                            'post_type' => 'maker',
                            'posts_per_page' => -1,
                            'orderby' => 'title',
                            'meta_query' => array(
                                array(
                                    'key' => 'features',
                                    'compare' => 'LIKE',
                                    'value' => '"' . get_the_id() . '"',
                                )
                            ),
                        'order' => 'ASC',
                        );

                        $makers = new WP_Query($args);
                        
                        while ($makers->have_posts() ) {
                            $makers->the_post();
                            
                            $image = get_field('image');
                    ?>
                
                    <!-- Search Widget -->
                        <div class="card-body">
                            <div class="card my-4">
                                <h5 class="card-header"><center><a href="<?php the_permalink(); ?>"> <?php the_title();?> </a></center></h5>
                                <div class="card-body">
                                    <center>
                                    <img style="width:200px;height:200px; object-fit:cover;" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                    </center>
                                </div>
                            </div>
                    </div>
                    <!-- Side Widget -->
                <?php
                }
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
<?php
}
get_footer();
?>