<?php

get_header();

?>
    <section class="page-section">
    <div class="container">
        <h1 class="my-4">
            <center>Listado de Novedades</center>
            <hr>
            <hr>
            <a href="<?php echo site_url("/") ?>" class="btn btn-read"><- Regresar</a>
        </h1>
        <div class="row mainrow">
            <div class="row tm-catalog-item-list">

                <?php

                // Listado de Novedades Custom Query
                $args = array(
                    'post_type' => 'feature',
                    'posts_per_page' => -1,
                    //'orderby' => 'p',
                    'order' => 'ASC',
                );

                $features = new WP_Query($args);

                while ($features->have_posts() ) {
                    $features->the_post();
                    $image = get_field('front_image');

                ?>
                    <!-- Marketing Icons Section -->
                    <div class="col-lg-4 col-md-6 col-sm-12 tm-catalog-item">
                        <div class="card h-100">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="img-content">
                            <div class="p-4 tm-bg-gray tm-catalog-item-description">
                                <h3 class="tm-text-primary mb-3 card-header h-bgcolor"><a href="<?php the_permalink(); ?>" style="color:brown;"> <?php the_title();   ?> </a></h3>
                                <p class="tm-catalog-item-text">
                                    <?php echo wp_trim_words(get_the_content(), 12); ?>
                                </p>
                                <div class="card-footer f-bgcolor">
                                    <a href="<?php the_permalink(); ?>" class="btn btn-read">Leer</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                }
                wp_reset_postdata();
                ?>
                
            </div>
            <div>
        </div>
        </div>
        <!-- /.row -->
        <!-- Catalog Paging Buttons -->
        

    </div>
</section>
<!-- /.container -->

<?php

get_footer();
?>

<style>
    .mainrow {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: 85px
    }

    .img-content{
        width: 100%;
        height: 176px
    }

    .btn-read{
        color: #fff;
        background-color: darkcyan;
        border-color: #206f65;

    }
    .h-bgcolor, .f-bgcolor{
        background-color: white;
    }
</style>
