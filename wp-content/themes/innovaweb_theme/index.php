<?php get_header(); ?>

<!-- Page Content -->
<section class="page-section">
    <div class="container">
        <h1 class="my-4 main-title">
            <center>Bienvenid@ a Innovaweb</center>
        </h1>
    </div>
    <hr>
    <hr>
    <div class="container">
                                    <!-- /////////// NOVEDADES /////////// -->
        <div class="row col-12">
            <div class="col-12">
            <div class="card h-100 h-title">
                <h2 class="tm-page-title">Novedades</h2>
            </div>
            </div>
        </div>
        <div class="row mainrow">
            <div class="row tm-catalog-item-list">

                        <?php

                            //Novedades
                            $args = array(
                                'post_type' => 'feature',
                                'posts_per_page' => 3,
                                'order' => 'ASC',
                            );

                            $features = new WP_Query($args);

                            while ($features->have_posts() ) {
                                $features->the_post();

                                $image = get_field('front_image');

                                /* if(is_null($image) || $image == ""){
                                    $image == "";
                                }else {
                                    $image = $image['size']['thumbnail'];
                                } */
                            
                        ?>
                    <!-- Marketing Icons Section -->
                    <div class="col-lg-4 col-md-6 col-sm-12 tm-catalog-item">
                        <div class="card h-100">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="img-content">
                            <div class="p-4 tm-bg-gray tm-catalog-item-description">
                                <h3 class="tm-text-primary mb-3 card-header h-bgcolor"><a href="<?php the_permalink(); ?>" style="color:brown;"> <?php the_title();   ?> </a></h3>
                                <p class="tm-catalog-item-text">
                                    <?php echo wp_trim_words(get_the_content(), 12); ?>
                                </p>
                                <div class="card-footer f-bgcolor">
                                    <a href="<?php the_permalink(); ?>" class="btn btn-read">Leer</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                }
                wp_reset_postdata();
                ?>
            </div>
            <div>
        </div>
        
        </div>
        <a style="text-align:right;color: darkcyan;" href="<?php echo site_url("/features") ?>" class="nav-link tm-paging-link"><h4>Ver Más...</h4></a>
        <!-- /.row -->
        <!-- Catalog Paging Buttons -->
        </div>
        <hr>
                                    <!-- /////////// CREADORES /////////// -->
        <hr>
        <div class="container">
        <div class="row col-12">
            <div class="col-12">
            <div class="card h-100 h-title">
                <h2 class="tm-page-title">Creadores</h2>
            </div>
            </div>
        </div>
        <div class="row mainrow">
            <div class="row tm-catalog-item-list">

                        <?php

                            //Creadores
                            $args = array(
                                'post_type' => 'maker',
                                'posts_per_page' => 3,
                                'order' => 'ASC',
                            );

                            $makers = new WP_Query($args);

                            while ($makers->have_posts() ) {
                                $makers->the_post();

                                $image = get_field('image');
                                
                            
                        ?>
                    <!-- Marketing Icons Section -->
                    <div class="col-lg-4 col-md-6 col-sm-12 tm-catalog-item">
                        <div class="card h-100">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="img-content">
                            <div class="p-4 tm-bg-gray tm-catalog-item-description">
                                <h3 class="tm-text-primary mb-3 card-header h-bgcolor"><a href="<?php the_permalink(); ?>" style="color:brown;"> <?php the_title();   ?> </a></h3>
                                <p class="tm-catalog-item-text">
                                    <?php echo wp_trim_words(get_the_content(), 12); ?>
                                </p>
                                <div class="card-footer f-bgcolor">
                                    <a href="<?php the_permalink(); ?>" class="btn btn-read">Leer</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                }
                wp_reset_postdata();
                ?>
            </div>
            <div>
        </div>
        </div>
        <a style="text-align:right;color: darkcyan;" href="<?php echo site_url("/makers") ?>" class="nav-link tm-paging-link"><h4>Ver Más...</h4></a>
        <!-- /.row -->
        <!-- Catalog Paging Buttons -->
        <hr>
        

    </div>
</section>
<!-- /.container -->

<?php get_footer(); ?>

<style>
    .mainrow {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: 85px
    }

    .img-content{
        width: 100%;
        height: 176px
    }

    .h-title{
        bottom: 10px;
    }

    .tm-page-title{
        color: darkcyan;
        text-align: center;
        font-size: -webkit-xxx-large;
        font-style: italic;
        font-family: monospace;
    }
    .main-title{
        text-align: center;
        font-size: -webkit-xxx-large;
        font-style: italic;
        font-family: monospace;
    }

    .btn-read{
        color: #fff;
        background-color: darkcyan;
        border-color: #206f65;

    }
    .h-bgcolor, .f-bgcolor{
        background-color: white;
    }
</style>