<?php
get_header();

while (have_posts()) {
    the_post();

?>
    <div class="container">
        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8 column_single">
                <!-- Title -->
                <hr>
                <h1>
                    <center><?php the_title(); ?></center>
                </h1>
                <hr>

                <!-- Post Content -->
                <p><center><?php the_content() ?></center></p>

                <!-- Date/Time -->
                <hr>
                <p><b>¿Cuándo se unió al equipo?:</b> <?php echo the_date(); ?></br>
                <div style="text-align:right"><?php previous_post_link(); echo "  ||  "; next_post_link();?></div>
                <hr>
                    <center><a href="<?php echo site_url("/makers") ?>" class="btn btn-primary">Regresar</a></center>
                </p>
                <hr>

            </div>
                <!-- Sidebar Widgets Column -->
            <div class="col-md-4 column_single">
                    
                    <?php

                        //Creadores Custom Query
                        $args = array(
                            'post_type' => 'feature',
                            'posts_per_page' => 1,
                            'orderby' => 'title',
                            'meta_query' => array(
                                array(
                                    'key' => 'maker',
                                    'compare' => 'LIKE',
                                    'value' => '"' . get_the_id() . '"',
                                )
                            ),
                        'order' => 'DCS',
                        );

                        $features = new WP_Query($args);
                        
                        while ($features->have_posts() ) {
                            $features->the_post();
                            
                            $image = get_field('image');
                    ?>
                
                    <!-- Search Widget -->
                        <div class="card-body">
                            <div class="card my-4">
                                <h5 class="card-header"><center>Último Post</center></h5>
                                <div class="card-body">
                                    <center><a href="<?php the_permalink(); ?>"> <?php the_title();?> </a></center>
                                    <center>
                                    <img style="width:200px;height:200px; object-fit:cover;" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                    </center>
                                </div>
                            </div>
                    </div>
                    <!-- Side Widget -->
                <?php
                }
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
<?php
}
get_footer();
?>