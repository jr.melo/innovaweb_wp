<?php 

function innovaweb_custom_post(){

    register_post_type('feature',
        array(
            'supports' => array('title', 'editor', 'thumbnail'),
            'rewrite' => array(
                'slug' => 'features'
            ),
            'labels'      => array(
                'name'          => __('Novedades', 'textdomain'),
                'singular_name' => __('Novedad', 'textdomain'),
            ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-media-interactive'
        )
    );


    register_post_type('maker',
        array(
            'supports' => array('title', 'editor', 'thumbnail'),
            'rewrite' => array(
                'slug' => 'makers'
            ),
            'labels'      => array(
                'name'          => __('Creadores', 'textdomain'),
                'singular_name' => __('Creador', 'textdomain'),
            ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-admin-customizer'
        )
    );

}

add_action('init', 'innovaweb_custom_post');